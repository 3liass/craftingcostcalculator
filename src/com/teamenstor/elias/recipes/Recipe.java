package com.teamenstor.elias.recipes;

import com.teamenstor.elias.materials.Material;
import lombok.Data;

import java.util.HashMap;

@Data
public class Recipe {

    private RecipeType type;

    private HashMap<Material.List, Integer> contents = new HashMap<>();

    public Recipe(RecipeType type, RecipePart... parts){

        this.type = type;
        for (RecipePart part : parts) {
            contents.put(part.getMaterial(), part.getAmount());
        }

    }

}
