package com.teamenstor.elias.recipes;

import com.teamenstor.elias.materials.Material;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

@Data
public class RecipePart {

    @NonNull private Material.List material;
    @NonNull private int amount;

    public static RecipePart create(Material.List material, int amount){
        return new RecipePart(material, amount);
    }

}
