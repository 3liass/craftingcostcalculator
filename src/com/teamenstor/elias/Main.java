package com.teamenstor.elias;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.MaterialUtility;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
 Hej!
 Välkommen till min kod <o/
 Detta är bara ett litet roligt projekt för att räkna ut hur mycket vissa föremål hade kostat på SentiaCraft :)

 Gjort av mig! (detta projektet alltså, inte SentiaCraft lol. Sentia är gjort av Slime!)
 Discord: Elias❤#9976
 Minecraft: 3liass


  c: <3
 */

public class Main {

    public static void main(String[] args) {

        Material.registerMaterials();

        Scanner scanner = new Scanner(System.in);

        System.out.println("""
                    Welcome to Crafting Cost Calculator!
                    Developed by 3liass.
                                        
                    Type "help" for a list of commands!
                    """);

        while(true){

            System.out.print("\n> ");

            String input = scanner.nextLine();

            List<String> rawArguments = new java.util.ArrayList<>(List.of(input.split(" ")));
            List<String> arguments = rawArguments.stream().filter(s -> !s.isEmpty() && !s.equals(" ")).collect(Collectors.toList());

            if(arguments.isEmpty()){
                System.out.println("Syntax error.");
                System.out.println("Type \"help\" for a list of commands.");
            }else

                /*
                Varning! Kommandona är inte gjorda på något vackert sätt för jag är lat. Det var allt, tack för mig.
                 */

            switch(arguments.get(0).replace(" ", "")){
                case "stop", "end" -> {
                    System.out.println("Bye!");
                    System.exit(0);
                }

                case "list", "ls" -> {
                    Material.getMaterials().values().stream().forEach(value -> System.out.println(value.getName() + " (" + value.getId() + ")"));
                }

                case "lookup", "l" -> {

                    arguments.remove(0);

                    List<String> materials = Material.getMaterials().keySet().stream().map(Enum::toString).collect(Collectors.toList());

                    switch (arguments.size()){
                        case 0 -> {
                            //lookup
                            System.out.println("Insufficient arguments!");
                            System.out.println("Type \"help\" for a list of commands.");
                        }
                        case 1 -> {
                            //lookup <material>

                            String materialName = arguments.get(0);


                            if(materials.contains(materialName.toUpperCase())){
                                MaterialUtility.printMaterialInfo(Material.getMaterial(Material.List.valueOf(materialName.toUpperCase())), 1);
                            }else{
                                System.out.println("There is no register material with the name \"" + materialName + "\"");
                                System.out.println("Type \"list\" for a list of materials.");
                            }

                        }
                        case 2 -> {
                            //lookup <material> [amount]

                            String materialName = arguments.get(0);
                            String rawAmount = arguments.get(1);

                            if(materials.contains(materialName.toUpperCase())){
                                if(rawAmount.contains("x")){

                                    List<String> amountArgs = List.of(rawAmount.split("x"));

                                    if(amountArgs.size()==2) {

                                        if (amountArgs.get(0).matches("-?(0|[1-9]\\d*)")) {
                                            int amount = Integer.parseInt(amountArgs.get(0));

                                            if (amountArgs.get(1).contains("+")) {
                                                List<String> addAmount = List.of(amountArgs.get(1).split("[+]"));
                                                if(addAmount.size()==2){
                                                    if(addAmount.get(0).matches("-?(0|[1-9]\\d*)")){
                                                        if(addAmount.get(1).matches("-?(0|[1-9]\\d*)")){
                                                            int stackSize = Integer.parseInt(addAmount.get(0));
                                                            int addNum = Integer.parseInt(addAmount.get(1));
                                                            MaterialUtility.printMaterialInfo(Material.getMaterial(Material.List.valueOf(materialName.toUpperCase())),
                                                                    Math.abs((amount*stackSize)+addNum));
                                                        }else{
                                                            System.out.println("Unknown number \"" + addAmount.get(1) + "\"");
                                                            System.out.println("Type \"help\" for a list of commands.");
                                                        }
                                                    }else{
                                                        System.out.println("Unknown number \"" + addAmount.get(0) + "\"");
                                                        System.out.println("Type \"help\" for a list of commands.");
                                                    }
                                                }else{
                                                    System.out.println("Unknown number \"" + rawAmount + "\"");
                                                    System.out.println("Type \"help\" for a list of commands.");
                                                }
                                            }else{
                                                if(amountArgs.get(1).matches("-?(0|[1-9]\\d*)")){
                                                    int stackSize = Integer.parseInt(amountArgs.get(1));
                                                    MaterialUtility.printMaterialInfo(Material.getMaterial(Material.List.valueOf(materialName.toUpperCase())),
                                                            Math.abs(amount*stackSize));
                                                }else{
                                                    System.out.println("Unknown number \"" + amountArgs.get(1) + "\"");
                                                    System.out.println("Type \"help\" for a list of commands.");
                                                }
                                            }
                                        } else {
                                            System.out.println("Unknown number \"" + amountArgs.get(0) + "\"");
                                            System.out.println("Type \"help\" for a list of commands.");
                                        }
                                    }else{
                                        System.out.println("Unknown number \"" + rawAmount + "\"");
                                        System.out.println("Type \"help\" for a list of commands.");
                                    }

                                }else if(rawAmount.matches("-?(0|[1-9]\\d*)")) {
                                    MaterialUtility.printMaterialInfo(Material.getMaterial(Material.List.valueOf(materialName.toUpperCase())), Math.abs(Integer.parseInt(rawAmount)));
                                }else{
                                    System.out.println("Unknown number \"" + rawAmount + "\"");
                                    System.out.println("Type \"help\" for a list of commands.");
                                }
                            }else{
                                System.out.println("There is no register material with the name \"" + materialName + "\"");
                                System.out.println("Type \"list\" for a list of materials.");
                            }
                        }
                        default -> {
                            System.out.println("Too many arguments.");
                            System.out.println("Type \"help\" for a list of commands.");
                        }
                    }


                }

                case "help" -> {
                    System.out.println("""
                            List of commands for Crafting Cost Calculator:
                            
                            help -> Shows a list of commands
                            
                            stop -> Ends the program
                            end -> alias for "stop"
                             
                            list -> Shows a list of all registered materials and their names
                            ls -> alias for "list"
                            lookup <material> [amount] -> Shows information about a given material.
                                    Amount can be written as just a number, but also as AxB or AxB+C
                            l -> alias for "lookup"
                            
                            """);
                }
                default -> {
                    System.out.println("Unknown command \"" + input + "\"");
                    System.out.println("Type \"help\" for a list of commands.");
                }
            }

            arguments.clear();

        }

    }
}
