package com.teamenstor.elias.materials.base.farming;

import com.teamenstor.elias.materials.Material;

public class Warped_Fungus extends Material {
    public Warped_Fungus() {
        super("warped_fungus", "Warped Fungus", null,
                List.WARPED_FUNGUS, -1, -1, null, 64);
    }
}
