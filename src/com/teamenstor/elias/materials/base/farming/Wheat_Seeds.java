package com.teamenstor.elias.materials.base.farming;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Wheat_Seeds extends Material {
    public Wheat_Seeds() {
        super("wheat_seeds", "Wheat Seeds", null,
                List.WHEAT_SEEDS, PriceUtility.getPrice(16, 5), PriceUtility.getPrice(16, 235), null, 64);
    }
}
