package com.teamenstor.elias.materials.base.farming;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Wheat extends Material {
    public Wheat() {
        super("wheat", "Wheat", null,
                List.WHEAT, PriceUtility.getPrice(16, 12), PriceUtility.getPrice(16, 595), null, 64);
    }
}
