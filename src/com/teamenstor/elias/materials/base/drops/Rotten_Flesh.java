package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Rotten_Flesh extends Material {
    public Rotten_Flesh() {
        super("rotten_flesh", "Rotten Flesh", null,
                List.ROTTEN_FLESH, PriceUtility.getPrice(16, 3), PriceUtility.getPrice(16, 325), null, 64);
    }
}
