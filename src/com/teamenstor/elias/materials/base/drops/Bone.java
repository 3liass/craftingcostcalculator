package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Bone extends Material {
    public Bone() {
        super("bone", "Bone", null,
                List.BONE, PriceUtility.getPrice(16, 3), PriceUtility.getPrice(16, 325), null, 64);
    }
}
