package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Ender_Pearl extends Material {
    public Ender_Pearl() {
        super("ender_pearl", "Ender Pearl", null,
                List.ENDER_PEARL, PriceUtility.getPrice(16, 3), PriceUtility.getPrice(16, 1005), null, 16);
    }
}
