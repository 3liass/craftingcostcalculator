package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Blaze_Rod extends Material {
    public Blaze_Rod() {
        super("blaze_rod", "Blaze Rod", null,
                List.BLAZE_ROD, PriceUtility.getPrice(64, 8), PriceUtility.getPrice(64, 1805), null, 64);
    }
}
