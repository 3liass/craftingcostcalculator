package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;

public class Phantom_Membrane extends Material {
    public Phantom_Membrane() {
        super("phantom_membrane", "Phantom Membrane", null,
                List.PHANTOM_MEMBRANE, -1, -1, null, 64);
    }
}
