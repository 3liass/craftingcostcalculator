package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class StringMaterial extends Material {
    public StringMaterial() {
        super("string", "String", null,
                List.STRING, PriceUtility.getPrice(16, 3), PriceUtility.getPrice(16, 425), null, 64);
    }
}
