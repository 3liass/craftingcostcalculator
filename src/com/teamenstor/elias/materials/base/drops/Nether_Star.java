package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;

public class Nether_Star extends Material {
    public Nether_Star() {
        super("nether_star", "Nether Star", null,
                List.NETHER_STAR, -1, -1, null, 64);
    }
}
