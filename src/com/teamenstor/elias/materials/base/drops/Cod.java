package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Cod extends Material {
    public Cod() {
        super("cod", "Raw Cod", null,
                List.COD, PriceUtility.getPrice(4, 4), PriceUtility.getPrice(4, 540), null, 64);
    }
}
