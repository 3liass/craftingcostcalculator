package com.teamenstor.elias.materials.base.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Rabbit_Foot extends Material {
    public Rabbit_Foot() {
        super("rabbit_foot", "Rabbit's Foot", null,
                List.RABBIT_FOOT, PriceUtility.getPrice(1, 5), PriceUtility.getPrice(1, 295), null, 64);
    }
}
