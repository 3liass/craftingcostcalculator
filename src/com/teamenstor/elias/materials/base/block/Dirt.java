package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Dirt extends Material {
    public Dirt() {
        super("dirt", "Dirt", null,
                List.DIRT, PriceUtility.getPrice(64, 3), PriceUtility.getPrice(64, 230), null, 64);
    }
}
