package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Coal_Block extends Material {


    public Coal_Block() {
        super("coal_block", "Coal Block", new Recipe(RecipeType.FINAL, RecipePart.create(List.COAL, 9)),
                List.COAL_BLOCK, -1, -1, null, 64);
    }
}
