package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Glass extends Material {

    public Glass() {
        super("glass", "Glass", null,
                List.GLASS, PriceUtility.getPrice(16, 4), PriceUtility.getPrice(16, 545), null, 64);
    }
}
