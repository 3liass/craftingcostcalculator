package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;

public class Stripped_Acacia_Log extends Material {


    public Stripped_Acacia_Log() {
        super("stripped_acacia_log", "Stripped Acacia Log", null,
                List.STRIPPED_ACACIA_LOG, -1, -1, Material.getMaterial(List.ACACIA_LOG), 64);
    }
}
