package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Bone_Block extends Material {
    public Bone_Block() {
        super("bone_block", "Bone Block", new Recipe(RecipeType.FINAL, RecipePart.create(List.BONE, 3)),
                List.BONE_BLOCK, -1, -1, null, 64);
    }
}
