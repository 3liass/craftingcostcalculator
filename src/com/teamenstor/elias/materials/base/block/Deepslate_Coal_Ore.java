package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;

public class Deepslate_Coal_Ore extends Material {
    public Deepslate_Coal_Ore() {
        super("deepslate_coal_ore", "Deepslate Coal Ore", null,
                List.DEEPSLATE_COAL_ORE, -1, -1, Material.getMaterial(List.COAL), 64);
    }
}
