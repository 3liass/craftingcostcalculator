package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;

public class Flowering_Azalea_Leaves extends Material {


    public Flowering_Azalea_Leaves() {
        super("flowering_azalea_leaves", "Flowering Azalea Leaves",  null,
                List.FLOWERING_AZALEA_LEAVES, -1, -1, null, 64);
    }
}
