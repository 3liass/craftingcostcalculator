package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;

public class Honey_Block extends Material {


    public Honey_Block() {
        super("honey_block", "Honey Block",  null,
                List.HONEY_BLOCK, -1, -1, null, 64);
    }
}
