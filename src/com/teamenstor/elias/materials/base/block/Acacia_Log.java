package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Acacia_Log extends Material {
    public Acacia_Log() {
        super("acacia_log", "Acacia Log", null,
                List.ACACIA_LOG, PriceUtility.getPrice(32, 23), PriceUtility.getPrice(32, 505), null, 64);
    }
}
