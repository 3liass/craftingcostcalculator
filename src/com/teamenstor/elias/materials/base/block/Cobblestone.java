package com.teamenstor.elias.materials.base.block;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Cobblestone extends Material {

    public Cobblestone() {
        super("cobblestone", "Cobblestone", null,
                List.COBBLESTONE, PriceUtility.getPrice(64, 3), PriceUtility.getPrice(64, 435), null, 64);
    }

}
