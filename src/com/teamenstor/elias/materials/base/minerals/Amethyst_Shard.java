package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Amethyst_Shard extends Material {

    public Amethyst_Shard() {
        super("amethyst_shard", "Amethyst Shard", null,
                List.AMETHYST_SHARD, PriceUtility.getPrice(2, 1), PriceUtility.getPrice(2, 850), null, 64);
    }
}
