package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;

public class Raw_Copper extends Material {
    public Raw_Copper() {
        super("raw_copper", "Raw Copper", null,
                List.RAW_COPPER, -1, -1, Material.getMaterial(List.COPPER_INGOT), 64);
    }
}
