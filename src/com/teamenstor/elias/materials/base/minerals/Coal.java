package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Coal extends Material {
    public Coal() {
        super("coal", "Coal", null,
                List.COAL, PriceUtility.getPrice(16, 10), PriceUtility.getPrice(16, 750), null, 64);
    }
}
