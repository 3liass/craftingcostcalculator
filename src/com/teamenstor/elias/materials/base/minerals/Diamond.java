package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Diamond extends Material {
    public Diamond() {
        super("diamond", "Diamond", null,
                List.DIAMOND, PriceUtility.getPrice(4, 75), PriceUtility.getPrice(4, 5450), null, 64);
    }
}
