package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Iron_Ingot extends Material {
    public Iron_Ingot() {
        super("iron_ingot", "Iron Ingot", null,
                List.IRON_INGOT, PriceUtility.getPrice(8, 4), PriceUtility.getPrice(8, 950), null, 64);
    }
}
