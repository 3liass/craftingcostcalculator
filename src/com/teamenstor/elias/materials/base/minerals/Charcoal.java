package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;

public class Charcoal extends Material {
    public Charcoal() {
        super("charcoal", "Charcoal", null,
                List.CHARCOAL, -1, -1, Material.getMaterial(List.COAL), 64);
    }
}
