package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;

public class Raw_Iron extends Material {
    public Raw_Iron() {
        super("raw_iron", "Raw Iron", null,
                List.RAW_IRON, -1, -1, Material.getMaterial(List.IRON_INGOT), 64);
    }
}
