package com.teamenstor.elias.materials.base.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.utility.PriceUtility;

public class Copper_Ingot extends Material {

    public Copper_Ingot() {
        super("copper_ingot", "Copper Ingot", null,
                List.COPPER_INGOT, PriceUtility.getPrice(8, 4), PriceUtility.getPrice(8, 1000), null, 64);
    }
}
