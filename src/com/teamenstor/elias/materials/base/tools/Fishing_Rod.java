package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;

public class Fishing_Rod extends Material {
    public Fishing_Rod() {
        super("fishing_rod", "Fishing Rod", null,
                List.FISHING_ROD, -1, -1, null, 64);
    }
}
