package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;

public class Stone_Hoe extends Material {
    public Stone_Hoe() {
        super("stone_hoe", "Stone Hoe", null,
                List.STONE_HOE, -1, -1, null, 64);
    }
}
