package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;

public class Stone_Axe extends Material {
    public Stone_Axe() {
        super("stone_axe", "Stone Axe", null,
                List.STONE_AXE, -1, -1, null, 64);
    }
}
