package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;

public class Stone_Pickaxe extends Material {
    public Stone_Pickaxe() {
        super("stone_pickaxe", "Stone Pickaxe", null,
                List.STONE_PICKAXE, -1, -1, null, 64);
    }
}
