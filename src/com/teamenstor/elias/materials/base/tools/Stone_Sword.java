package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;

public class Stone_Sword extends Material {
    public Stone_Sword() {
        super("stone_sword", "Stonen Sword", null,
                List.STONE_SWORD, -1, -1, null, 64);
    }
}
