package com.teamenstor.elias.materials.base.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Water_Bucket extends Material {
    public Water_Bucket() {
        super("water_bucket", "Water Bucket", new Recipe(RecipeType.FINAL, RecipePart.create(List.IRON_INGOT, 3)),
                List.WATER_BUCKET, -1, -1, null, 64);
    }
}
