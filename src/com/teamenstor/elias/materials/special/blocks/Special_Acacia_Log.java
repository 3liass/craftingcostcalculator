package com.teamenstor.elias.materials.special.blocks;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Acacia_Log extends Material {
    public Special_Acacia_Log() {
        super("special_acacia_log", "Special Acacia Log", new Recipe(RecipeType.FINAL, RecipePart.create(List.ACACIA_LOG, 64),
                        RecipePart.create(List.STRIPPED_ACACIA_LOG, 64*4)),
                List.SPECIAL_ACACIA_LOG, -1, -1, null, 64);
    }
}
