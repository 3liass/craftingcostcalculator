package com.teamenstor.elias.materials.special.blocks;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Hardened_Glass extends Material {
    public Hardened_Glass() {
        super("hardened_glass", "Hardened Glass", new Recipe(RecipeType.FINAL, RecipePart.create(List.GLASS, 1)),
                List.HARDENED_GLASS, -1, -1, null, 64);
    }
}
