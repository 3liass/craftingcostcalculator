package com.teamenstor.elias.materials.special.blocks;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Extra_Flowery_Azalea_Leaves extends Material {
    public Extra_Flowery_Azalea_Leaves() {
        super("extra_flowery_azalea_leaves", "Extra Flowery Azalea Leaves", new Recipe(RecipeType.FINAL, RecipePart.create(List.FLOWERING_AZALEA_LEAVES, 64*8)),
                List.EXTRA_FLOWERY_AZALEA_LEAVES, -1, -1, null, 64);
    }
}
