package com.teamenstor.elias.materials.special.blocks;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Honey_Block extends Material {
    public Special_Honey_Block() {
        super("special_honey_block", "Special Honey Block", new Recipe(RecipeType.FINAL, RecipePart.create(List.HONEY_BLOCK, 4*4)),
                List.SPECIAL_HONEY_BLOCK, -1, -1, null, 64);
    }
}
