package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Cod extends Material {
    public Special_Cod() {
        super("special_cod", "Special Cod", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.COD, 64*4),
                        RecipePart.create(List.WATER_BUCKET, 1)),
                List.SPECIAL_COD, -1, -1, null, 64);
    }
}
