package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Bone extends Material {
    public Special_Bone() {
        super("special_bone", "Special Bone", new Recipe(RecipeType.FINAL, RecipePart.create(List.BONE_BLOCK, 64),
                        RecipePart.create(List.BONE, 64*4)),
                List.SPECIAL_BONE, -1, -1, null, 64);
    }
}
