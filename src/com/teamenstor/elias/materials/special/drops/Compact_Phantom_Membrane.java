package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Compact_Phantom_Membrane extends Material {
    public Compact_Phantom_Membrane() {
        super("compact_phantom_membrane", "Compact Phantom Membrane", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.PHANTOM_MEMBRANE, 32*4),
                        RecipePart.create(List.TOUGH_STRING, 8)),
                List.COMPACT_PHANTOM_MEMBRANE, -1, -1, null, 64);
    }
}
