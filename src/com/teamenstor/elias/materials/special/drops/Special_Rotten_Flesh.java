package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Rotten_Flesh extends Material {
    public Special_Rotten_Flesh() {
        super("special_rotten_flesh", "Special Rotten Flesh", new Recipe(RecipeType.FINAL, RecipePart.create(List.ROTTEN_FLESH, 64*4),
                        RecipePart.create(List.RABBIT_FOOT, 1)),
                List.SPECIAL_ROTTEN_FLESH, -1, -1, null, 64);
    }
}
