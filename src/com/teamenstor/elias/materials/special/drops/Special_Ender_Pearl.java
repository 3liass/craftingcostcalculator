package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Ender_Pearl extends Material {
    public Special_Ender_Pearl() {
        super("special_ender_pearl", "Special Ender Pearl", new Recipe(RecipeType.FINAL, RecipePart.create(List.ENDER_PEARL, 16*5)),
                List.SPECIAL_ENDER_PEARL, -1, -1, null, 16);
    }
}
