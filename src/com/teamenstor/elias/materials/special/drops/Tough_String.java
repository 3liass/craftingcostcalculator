package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Tough_String extends Material {
    public Tough_String() {
        super("tough_string", "Tough String", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.STRING, 64*8),
                        RecipePart.create(List.SPECIAL_IRON_INGOT, 1)),
                List.TOUGH_STRING, -1, -1, null, 64);
    }
}
