package com.teamenstor.elias.materials.special.drops;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Blaze_Rod extends Material {
    public Special_Blaze_Rod() {
        super("special_blaze_rod", "Special Blaze Rod", new Recipe(RecipeType.FINAL, RecipePart.create(List.BLAZE_ROD, 64*4)),
                List.SPECIAL_BLAZE_ROD, -1, -1, null, 64);
    }
}
