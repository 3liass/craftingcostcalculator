package com.teamenstor.elias.materials.special.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Stone_Hoe extends Material {
    public Loaded_Stone_Hoe() {
        super("loaded_stone_hoe", "Loaded Stone Hoe", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.STONE_HOE, 1),
                        RecipePart.create(List.NETHER_STAR, 3), RecipePart.create(List.SPECIAL_DIAMOND, 3*2)),
                List.LOADED_STONE_HOE, -1, -1, null, 64);
    }
}
