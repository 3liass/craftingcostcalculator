package com.teamenstor.elias.materials.special.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Stone_Sword extends Material {
    public Loaded_Stone_Sword() {
        super("loaded_stone_sword", "Loaded Stone Sword",  new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_DIAMOND, 2),
                        RecipePart.create(List.STONE_SWORD, 1), RecipePart.create(List.NETHER_STAR, 1)),
                List.LOADED_STONE_SWORD, -1, -1, null, 64);
    }
}
