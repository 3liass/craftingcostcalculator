package com.teamenstor.elias.materials.special.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Stone_Pickaxe extends Material {
    public Loaded_Stone_Pickaxe() {
        super("loaded_stone_pickaxe", "Loaded Stone Pickaxe", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_DIAMOND, 2),
                        RecipePart.create(List.NETHER_STAR, 3), RecipePart.create(List.STONE_PICKAXE, 1)),
                List.LOADED_STONE_PICKAXE, -1, -1, null, 64);
    }
}
