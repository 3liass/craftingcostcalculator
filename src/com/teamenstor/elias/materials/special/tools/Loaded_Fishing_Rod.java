package com.teamenstor.elias.materials.special.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Fishing_Rod extends Material {
    public Loaded_Fishing_Rod() {
        super("loaded_fishing_rod", "Loaded Fishing Rod", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.FISHING_ROD, 1),
                        RecipePart.create(List.NETHER_STAR, 2), RecipePart.create(List.SPECIAL_DIAMOND, 3)),
                List.LOADED_FISHING_ROD, -1, -1, null, 64);
    }
}
