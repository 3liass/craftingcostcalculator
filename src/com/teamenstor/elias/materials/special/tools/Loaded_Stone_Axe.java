package com.teamenstor.elias.materials.special.tools;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Stone_Axe extends Material {
    public Loaded_Stone_Axe() {
        super("loaded_stone_axe", "Loaded Stone Axe", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_DIAMOND, 3*2),
                        RecipePart.create(List.STONE_AXE, 1), RecipePart.create(List.NETHER_STAR, 3)),
                List.LOADED_STONE_AXE, -1, -1, null, 64);
    }
}
