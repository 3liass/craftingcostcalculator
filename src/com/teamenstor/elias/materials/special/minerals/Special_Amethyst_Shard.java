package com.teamenstor.elias.materials.special.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Amethyst_Shard extends Material {
    public Special_Amethyst_Shard() {
        super("special_amethyst_shard", "Special Amethyst Shard", new Recipe(RecipeType.FINAL, RecipePart.create(List.AMETHYST_SHARD, 48*4)),
                List.SPECIAL_AMETHYST_SHARD, -1, -1, null, 64);
    }
}
