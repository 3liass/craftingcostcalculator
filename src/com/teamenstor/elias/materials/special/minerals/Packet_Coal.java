package com.teamenstor.elias.materials.special.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Packet_Coal extends Material {
    public Packet_Coal() {
        super("packet_coal", "Packet Coal", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.COAL_BLOCK, 64),
                        RecipePart.create(List.LOADED_CHARCOAL, 4), RecipePart.create(List.SPECIAL_BLAZE_ROD, 4)),
                List.PACKET_COAL, -1, -1, null, 64);
    }
}
