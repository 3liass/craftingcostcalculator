package com.teamenstor.elias.materials.special.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Diamond extends Material {
    public Special_Diamond() {
        super("special_diamond", "Special Diamond", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_AMETHYST_SHARD, 4),
                        RecipePart.create(List.DIAMOND, 32*4), RecipePart.create(List.SPECIAL_ENDER_PEARL, 1)),
                List.SPECIAL_DIAMOND, -1, -1, null, 64);
    }
}
