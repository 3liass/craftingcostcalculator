package com.teamenstor.elias.materials.special.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Loaded_Charcoal extends Material {
    public Loaded_Charcoal() {
        super("loaded_charcoal", "Loaded Charcoal", new Recipe(RecipeType.FINAL, RecipePart.create(List.CHARCOAL, 64),
                        RecipePart.create(List.DEEPSLATE_COAL_ORE, 4*4)),
                List.LOADED_CHARCOAL, -1, -1, null, 64);
    }
}
