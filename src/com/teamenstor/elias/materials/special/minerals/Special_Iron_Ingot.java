package com.teamenstor.elias.materials.special.minerals;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Iron_Ingot extends Material {
    public Special_Iron_Ingot() {
        super("special_iron_ingot", "Special Iron Ingot", new Recipe(RecipeType.FINAL, RecipePart.create(List.RAW_IRON, 64*5),
                        RecipePart.create(List.RAW_COPPER, 64*4)),
                List.SPECIAL_IRON_INGOT, -1, -1, null, 64);
    }
}
