package com.teamenstor.elias.materials.special.other;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Full_Honey_Bottle extends Material {
    public Full_Honey_Bottle() {
        super("full_honey_bottle", "Full Honey Bottle", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_HONEY_BLOCK, 1),
                        RecipePart.create(List.HARDENED_GLASS, 3)),
                List.FULL_HONEY_BOTTLE, -1, -1, null, 64);
    }
}
