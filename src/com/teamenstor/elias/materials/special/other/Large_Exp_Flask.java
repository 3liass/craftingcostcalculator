package com.teamenstor.elias.materials.special.other;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Large_Exp_Flask extends Material {
    public Large_Exp_Flask() {
        super("large_exp_flask", "Large Exp Flask", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.SPECIAL_ENDER_PEARL, 3),
                        RecipePart.create(List.HARDENED_GLASS, 3*3), RecipePart.create(List.SPECIAL_HONEY_BLOCK, 1),
                        RecipePart.create(List.SPECIAL_BLAZE_ROD, 2)),
                List.LARGE_EXP_FLASK, -1, -1, null, 64);
    }
}
