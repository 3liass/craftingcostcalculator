package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Butcher_Minion extends Material {
    public Butcher_Minion() {
        super("butcher_minion", "Butcher Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_STONE_SWORD, 1),
                        RecipePart.create(List.SPECIAL_ROTTEN_FLESH, 16*4), RecipePart.create(List.SPECIAL_BONE, 16*4)),
                List.BUTCHER_MINION, -1, -1, null, 64);
    }
}
