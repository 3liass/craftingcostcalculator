package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Farmer_Minion extends Material {
    public Farmer_Minion() {
        super("farmer_minion", "Farmer Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_STONE_HOE, 1),
                        RecipePart.create(List.SPECIAL_FUNGUS, 4*4), RecipePart.create(List.SPECIAL_WHEAT, 64*4)),
                List.FARMER_MINION, -1, -1, null, 64);
    }
}
