package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Mining_Minion extends Material {
    public Mining_Minion() {
        super("mining_minion", "Mining Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_STONE_PICKAXE, 1),
                        RecipePart.create(List.SPECIAL_DIAMOND, 4*4), RecipePart.create(List.TOUGH_STRING, 8*4)),
                List.MINING_MINION, -1, -1, null, 64);
    }
}
