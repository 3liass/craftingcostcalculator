package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Lumberjack_Minion extends Material {
    public Lumberjack_Minion() {
        super("lumberjack_minion", "Lumberjack Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_STONE_AXE, 1),
                        RecipePart.create(List.SPECIAL_ACACIA_LOG, 8*4), RecipePart.create(List.EXTRA_FLOWERY_AZALEA_LEAVES, 16*4)),
                List.LUMBERJACK_MINION, -1, -1, null, 64);
    }
}
