package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Fuel_Minion extends Material {
    public Fuel_Minion() {
        super("fuel_minion", "Fuel Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_CHARCOAL, 1),
                        RecipePart.create(List.PACKET_COAL, 8*4), RecipePart.create(List.SPECIAL_BLAZE_ROD, 64*4)),
                List.FUEL_MINION, -1, -1, null, 64);
    }
}
