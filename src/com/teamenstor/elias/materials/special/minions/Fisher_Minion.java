package com.teamenstor.elias.materials.special.minions;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Fisher_Minion extends Material {
    public Fisher_Minion() {
        super("fisher_minion", "Fisher Minion", new Recipe(RecipeType.SPECIAL, RecipePart.create(List.LOADED_FISHING_ROD, 1),
                        RecipePart.create(List.TOUGH_STRING, 8*4), RecipePart.create(List.SPECIAL_COD, 32*4)),
                List.FISHER_MINION, -1, -1, null, 64);
    }
}
