package com.teamenstor.elias.materials.special.farming;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Fungus extends Material {
    public Special_Fungus() {
        super("special_fungus", "Special Fungus", new Recipe(RecipeType.FINAL, RecipePart.create(List.WARPED_FUNGUS, 64*4)),
                List.SPECIAL_FUNGUS, -1, -1, null, 64);
    }
}
