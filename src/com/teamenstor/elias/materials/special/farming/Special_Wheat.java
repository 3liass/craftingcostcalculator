package com.teamenstor.elias.materials.special.farming;

import com.teamenstor.elias.materials.Material;
import com.teamenstor.elias.recipes.Recipe;
import com.teamenstor.elias.recipes.RecipePart;
import com.teamenstor.elias.recipes.RecipeType;

public class Special_Wheat extends Material {
    public Special_Wheat() {
        super("special_wheat", "Special Wheat", new Recipe(RecipeType.FINAL, RecipePart.create(List.WHEAT, 32*4),
                        RecipePart.create(List.WHEAT_SEEDS, 64)),
                List.SPECIAL_WHEAT, -1, -1, null, 64);
    }
}
