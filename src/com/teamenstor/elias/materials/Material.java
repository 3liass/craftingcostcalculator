package com.teamenstor.elias.materials;

import com.teamenstor.elias.materials.base.block.*;
import com.teamenstor.elias.materials.base.drops.*;
import com.teamenstor.elias.materials.base.farming.Warped_Fungus;
import com.teamenstor.elias.materials.base.farming.Wheat;
import com.teamenstor.elias.materials.base.farming.Wheat_Seeds;
import com.teamenstor.elias.materials.base.minerals.*;
import com.teamenstor.elias.materials.base.tools.*;
import com.teamenstor.elias.materials.special.blocks.Extra_Flowery_Azalea_Leaves;
import com.teamenstor.elias.materials.special.blocks.Hardened_Glass;
import com.teamenstor.elias.materials.special.blocks.Special_Acacia_Log;
import com.teamenstor.elias.materials.special.blocks.Special_Honey_Block;
import com.teamenstor.elias.materials.special.drops.*;
import com.teamenstor.elias.materials.special.farming.Special_Fungus;
import com.teamenstor.elias.materials.special.farming.Special_Wheat;
import com.teamenstor.elias.materials.special.minerals.*;
import com.teamenstor.elias.materials.special.minions.*;
import com.teamenstor.elias.materials.special.other.Full_Honey_Bottle;
import com.teamenstor.elias.materials.special.other.Large_Exp_Flask;
import com.teamenstor.elias.materials.special.tools.*;
import com.teamenstor.elias.recipes.Recipe;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.HashMap;

@Data @AllArgsConstructor
public class Material {

    @NonNull private String id;

    @NonNull private String name;

    private Recipe recipe;

    @NonNull private List type;

    @NonNull private float sellPrice;
    @NonNull private float buyPrice;

    private Material priceConverter;

    private int stackSize;

    @Getter
    private static HashMap<List, Material> materials = new HashMap<>();

    public static Material getMaterial(List type){
        return materials.get(type);
    }

    public Material(@NonNull String id, @NonNull String name, Recipe recipe, @NonNull List type, @NonNull float sellPrice, @NonNull float buyPrice, Material priceConverter){
        this(id, name, recipe, type, sellPrice, buyPrice,  priceConverter, 64);
    }

    public enum List {

        /* --- "Base" Items --- */

        //Blocks
        COBBLESTONE, STRIPPED_ACACIA_LOG, ACACIA_LOG, FLOWERING_AZALEA_LEAVES, HONEY_BLOCK, GLASS, COAL_BLOCK, DEEPSLATE_COAL_ORE, BONE_BLOCK, DIRT,

        //Minerals
        IRON_INGOT, COPPER_INGOT, AMETHYST_SHARD, DIAMOND, GOLD_INGOT, QUARTZ, EMERALD, REDSTONE, COAL, GOLD_NUGGET, LAPIS_LAZULI, RAW_IRON, RAW_COPPER, CHARCOAL,

        //Mob drops
        BEEF, COD, SALMON, RABBIT_FOOT, ROTTEN_FLESH, BONE, STRING, ENDER_PEARL, SLIME_BALL, LEATHER, BLAZE_ROD, NETHER_STAR, PHANTOM_MEMBRANE,

        //Tools
        STONE_SWORD, STONE_PICKAXE, STONE_AXE, STONE_HOE, FISHING_ROD, WATER_BUCKET,

        //Farming
        WHEAT, WARPED_FUNGUS, WHEAT_SEEDS,


        /* --- Special Items --- */

        //Blocks
        HARDENED_GLASS, SPECIAL_HONEY_BLOCK, SPECIAL_ACACIA_LOG, EXTRA_FLOWERY_AZALEA_LEAVES,

        //Minerals
        SPECIAL_AMETHYST_SHARD, SPECIAL_DIAMOND, SPECIAL_IRON_INGOT, LOADED_CHARCOAL, PACKET_COAL,

        //Mob drops
        SPECIAL_ROTTEN_FLESH, SPECIAL_ENDER_PEARL, SPECIAL_BLAZE_ROD, SPECIAL_BONE, COMPACT_PHANTOM_MEMBRANE, TOUGH_STRING, SPECIAL_COD,

        //Tools
        LOADED_STONE_SWORD, LOADED_STONE_PICKAXE, LOADED_STONE_AXE, LOADED_STONE_HOE, LOADED_FISHING_ROD,

        //Farming
        SPECIAL_WHEAT, SPECIAL_FUNGUS,

        //Minions
        BUTCHER_MINION, MINING_MINION, LUMBERJACK_MINION, FARMER_MINION, FISHER_MINION, FUEL_MINION,

        //Other
        FULL_HONEY_BOTTLE, LARGE_EXP_FLASK,

    }

    private static void registerMaterial(List type, Material material){
        if(!getMaterials().containsKey(type)){
            getMaterials().put(type, material);
        }else{
            System.out.println("[Warning] Duplicate Material ID \"" + type.toString() + "\"!");
        }
    }

    public static void registerMaterials(){

        //blocks
        registerMaterial(List.COBBLESTONE, new Cobblestone());
        registerMaterial(List.STRIPPED_ACACIA_LOG, new Stripped_Acacia_Log());
        registerMaterial(List.ACACIA_LOG, new Acacia_Log());
        registerMaterial(List.FLOWERING_AZALEA_LEAVES, new Flowering_Azalea_Leaves());
        registerMaterial(List.HONEY_BLOCK, new Honey_Block());
        registerMaterial(List.GLASS, new Glass());
        registerMaterial(List.COAL_BLOCK, new Coal_Block());
        registerMaterial(List.DEEPSLATE_COAL_ORE, new Deepslate_Coal_Ore());
        registerMaterial(List.BONE_BLOCK, new Bone_Block());
        registerMaterial(List.DIRT, new Dirt());

        //minerals
        registerMaterial(List.IRON_INGOT, new Iron_Ingot());
        registerMaterial(List.COPPER_INGOT, new Copper_Ingot());
        registerMaterial(List.AMETHYST_SHARD, new Amethyst_Shard());
        registerMaterial(List.DIAMOND, new Diamond());
        registerMaterial(List.COAL, new Coal());
        registerMaterial(List.RAW_IRON, new Raw_Iron());
        registerMaterial(List.RAW_COPPER, new Raw_Copper());
        registerMaterial(List.CHARCOAL, new Charcoal());

        //drops
        registerMaterial(List.COD, new Cod());
        registerMaterial(List.RABBIT_FOOT, new Rabbit_Foot());
        registerMaterial(List.ROTTEN_FLESH, new Rotten_Flesh());
        registerMaterial(List.BONE, new Bone());
        registerMaterial(List.STRING, new StringMaterial());
        registerMaterial(List.ENDER_PEARL, new Ender_Pearl());
        registerMaterial(List.BLAZE_ROD, new Blaze_Rod());
        registerMaterial(List.NETHER_STAR, new Nether_Star());
        registerMaterial(List.PHANTOM_MEMBRANE, new Phantom_Membrane());

        //tools
        registerMaterial(List.STONE_SWORD, new Stone_Sword());
        registerMaterial(List.STONE_PICKAXE, new Stone_Pickaxe());
        registerMaterial(List.STONE_AXE, new Stone_Axe());
        registerMaterial(List.STONE_HOE, new Stone_Hoe());
        registerMaterial(List.FISHING_ROD, new Fishing_Rod());
        registerMaterial(List.WATER_BUCKET, new Water_Bucket());

        //farming
        registerMaterial(List.WHEAT, new Wheat());
        registerMaterial(List.WARPED_FUNGUS, new Warped_Fungus());
        registerMaterial(List.WHEAT_SEEDS, new Wheat_Seeds());

        //blocks
        registerMaterial(List.HARDENED_GLASS, new Hardened_Glass());
        registerMaterial(List.SPECIAL_HONEY_BLOCK, new Special_Honey_Block());
        registerMaterial(List.SPECIAL_ACACIA_LOG, new Special_Acacia_Log());
        registerMaterial(List.EXTRA_FLOWERY_AZALEA_LEAVES, new Extra_Flowery_Azalea_Leaves());

        //minerals
        registerMaterial(List.SPECIAL_AMETHYST_SHARD, new Special_Amethyst_Shard());
        registerMaterial(List.SPECIAL_DIAMOND, new Special_Diamond());
        registerMaterial(List.SPECIAL_IRON_INGOT, new Special_Iron_Ingot());
        registerMaterial(List.LOADED_CHARCOAL, new Loaded_Charcoal());
        registerMaterial(List.PACKET_COAL, new Packet_Coal());

        //drops
        registerMaterial(List.SPECIAL_ROTTEN_FLESH, new Special_Rotten_Flesh());
        registerMaterial(List.SPECIAL_ENDER_PEARL, new Special_Ender_Pearl());
        registerMaterial(List.SPECIAL_BLAZE_ROD, new Special_Blaze_Rod());
        registerMaterial(List.SPECIAL_BONE, new Special_Bone());
        registerMaterial(List.COMPACT_PHANTOM_MEMBRANE, new Compact_Phantom_Membrane());
        registerMaterial(List.TOUGH_STRING, new Tough_String());
        registerMaterial(List.SPECIAL_COD, new Special_Cod());

        //tools
        registerMaterial(List.LOADED_STONE_SWORD, new Loaded_Stone_Sword());
        registerMaterial(List.LOADED_STONE_PICKAXE, new Loaded_Stone_Pickaxe());
        registerMaterial(List.LOADED_STONE_AXE, new Loaded_Stone_Axe());
        registerMaterial(List.LOADED_STONE_HOE, new Loaded_Stone_Hoe());
        registerMaterial(List.LOADED_FISHING_ROD, new Loaded_Fishing_Rod());

        //farming
        registerMaterial(List.SPECIAL_WHEAT, new Special_Wheat());
        registerMaterial(List.SPECIAL_FUNGUS, new Special_Fungus());

        //minions
        registerMaterial(List.BUTCHER_MINION, new Butcher_Minion());
        registerMaterial(List.MINING_MINION, new Mining_Minion());
        registerMaterial(List.LUMBERJACK_MINION, new Lumberjack_Minion());
        registerMaterial(List.FARMER_MINION, new Farmer_Minion());
        registerMaterial(List.FISHER_MINION, new Fisher_Minion());
        registerMaterial(List.FUEL_MINION, new Fuel_Minion());

        //other
        registerMaterial(List.FULL_HONEY_BOTTLE, new Full_Honey_Bottle());
        registerMaterial(List.LARGE_EXP_FLASK, new Large_Exp_Flask());
    }
}
