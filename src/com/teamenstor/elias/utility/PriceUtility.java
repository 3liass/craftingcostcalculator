package com.teamenstor.elias.utility;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PriceUtility {

    public static float getPrice(float amount, float price){
        return price / amount;
    }

}
