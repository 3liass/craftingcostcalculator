package com.teamenstor.elias.utility;

import com.teamenstor.elias.materials.Material;
import lombok.experimental.UtilityClass;

import java.text.DecimalFormat;
import java.util.HashMap;

@UtilityClass
public class MaterialUtility {

    private static float totalSellPrice = 0, totalBuyPrice = 0;
    private static HashMap<Material, Integer> priceConverters;

    public static void printMaterialInfo(Material material, int amount){

        totalSellPrice = 0;
        totalBuyPrice = 0;
        priceConverters = new HashMap<>();

        System.out.printf("""
                
                -------------------------------------------------------------
                
                Material name: %1$s
                Material id: %2$s
                
                %3$s
                
                -------------------------------------------------------------
                
                """,
                material.getName(), material.getId(), getCraftingAndPrice(material, amount, true, 1));

    }

    public static void printMaterialInfo(Material.List material, int amount){
        printMaterialInfo(Material.getMaterial(material), amount);
    }

    private static String getFormattedAmount(Material material, int amount){

        StringBuilder builder = new StringBuilder();

        builder.append(" (");

        builder.append(amount);
        if(amount >= material.getStackSize()){
            builder.append(" = ").append(amount/material.getStackSize()).append("x").append(material.getStackSize());
            if(amount%material.getStackSize()>0) builder.append(" + ").append(amount%material.getStackSize());
        }

        builder.append(")");

        return builder.toString();
    }

    private static String getPrice(Material material, int amount){
        return (material.getSellPrice() == -1 ? "Item cannot be sold" : "Can be sold for " + getFormattedPrice(material.getSellPrice(), amount) + " kr") +
                "  |  " + (material.getBuyPrice() == -1 ? "Item cannot be bought" : "Can be bought for " + getFormattedPrice(material.getBuyPrice(), amount) + " kr");
    }

    private static String getFormattedPrice(float price, float amount){
        String pattern = "###,###.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);

        return decimalFormat.format(price*amount);
    }

    private static float getRawPrice(float price, float amount){
        if(price == -1) return 0;
        return price*amount;
    }

    private static String getCraftingAndPrice(Material material, int amount, boolean base, int step){
        StringBuilder builder = new StringBuilder();

        if(material.getRecipe() == null){
            if(base)
                builder.append("Item cannot be crafted.\n\n");
            builder.append(material.getName()).append(getFormattedAmount(material,amount));
            builder.append("  ").append(getPrice(material, amount));

            totalSellPrice+= getRawPrice(material.getSellPrice(), amount);
            totalBuyPrice+= getRawPrice(material.getBuyPrice(), amount);

            if(material.getPriceConverter()!=null) {
                if(priceConverters.containsKey(material))
                    priceConverters.put(material, priceConverters.get(material)+amount);
                else
                    priceConverters.put(material, amount);
            }
        }
        else{

            builder.append(material.getName()).append(getFormattedAmount(material,amount)).append(" ->");

            material.getRecipe().getContents().forEach((key, value) -> {

                builder.append("\n");
                builder.append("   | ".repeat(step));
                builder.append(getCraftingAndPrice(Material.getMaterial(key), value * amount, false, step + 1));

            });

        }

        if(base){

            if(totalSellPrice == 0)
                builder.append("\n\n\nNone of the items can be sold");
            else
                builder.append("\n\n\n").append("Total sell price: ").append(getFormattedPrice(totalSellPrice, 1)).append(" kr");

            if(totalBuyPrice == 0)
                builder.append("\nNone of the items can be bought");
            else
                builder.append("\n").append("Total buy price: ").append(getFormattedPrice(totalBuyPrice, 1)).append(" kr");


            if(priceConverters.size() > 0){
                builder.append("\n\n\nPrices converted:");
                priceConverters.forEach((key, value) -> {
                    builder.append("\n").append(key.getName()).append(" -> ").append(key.getPriceConverter().getName()).append(" (")
                            .append(getPrice(key.getPriceConverter(), value)).append(")");
                    totalSellPrice += getRawPrice(key.getPriceConverter().getSellPrice(), value);
                    totalBuyPrice += getRawPrice(key.getPriceConverter().getBuyPrice(), value);
                });

                builder.append("\n\n\n").append("Total sell price when converted: ").append(getFormattedPrice(totalSellPrice, 1)).append(" kr");
                builder.append("\n").append("Total buy price when converted: ").append(getFormattedPrice(totalBuyPrice, 1)).append(" kr");

            }
        }
        return builder.toString();
    }

}
